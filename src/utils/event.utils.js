const sortEvent = (a, b) => {
  if (a.startingHour === b.startingHour) {
    if (a.startingMinutes === b.startingMinutes) return b.duration - a.duration;
    return a.startingMinutes - b.startingMinutes;
  }
  return a.startingHour - b.startingHour;
};

const filterOverlap = (event, eventCompareTo) => {
  if (event.id === eventCompareTo.id) return false;
  if (
    event.startTime < eventCompareTo.endTime &&
    event.endTime > eventCompareTo.startTime
  )
    return true;
  return false;
};

const extractOverlap = (event, index, events) => {
  const overlap = events.filter((e) => filterOverlap(event, e));
  let multipleOverlap = false;
  for (let ev of overlap) {
    multipleOverlap = overlap.some((e) => filterOverlap(ev, e));
  }
  return { ...event, overlap, multipleOverlap };
};

const setDisplayQuotient = (acc, event, index, events) => {
  const overlap = events.filter((e) => filterOverlap(event, e));
  let multipleOverlap = false;
  for (let ev of overlap) {
    multipleOverlap = overlap.some((e) => filterOverlap(ev, e));
    if (multipleOverlap) break;
  }

  let shift = 0;
  if (overlap.length > 0) {
    if (
      overlap.length === 1 &&
      overlap.some((ev) => Object.hasOwn(ev, "shift") && ev.shift !== 0)
    )
      shift -= 1;

    for (let ev of overlap) {
      if (Object.hasOwn(ev, "shift")) shift += 1;
    }
  }

  let widthQuotient = 1;
  if (multipleOverlap) {
    if (!overlap.some((ev) => ev.multipleOverlap)) {
      widthQuotient = 2;
    } else {
      widthQuotient =
        overlap.filter((ov) => ov.startingHour === event.startingHour).length +
        1;
    }
  } else if (overlap) {
    widthQuotient = 2;
  }

  events[index] = {
    ...event,
    ovLength: overlap.length,
    overlap,
    shift,
    widthQuotient,
  };
  return events;
};

const setDisplayProperty = (event, hourHeight, minHour, windowWidth) => {
  const { duration, shift, widthQuotient } = event;
  const eventWidth = (windowWidth * 0.948) / (widthQuotient || 1);

  event.width = eventWidth;
  event.height = (duration / 60) * hourHeight + "px";
  event.left = eventWidth * shift || 0;
  event.top =
    hourHeight * event.startingHour -
    hourHeight * minHour +
    (event.startingMinutes / 60) * hourHeight +
    "px";
};
const extractEventTimes = (e) => {
  const [startingHour, startingMinutes] = e.start.split(":");
  const endMinutes = (e.duration + Number(startingMinutes)) % 60;
  const endHour =
    Number(startingHour) +
    Math.floor((e.duration + Number(startingMinutes)) / 60);
  const startTime = Number(startingHour) * 60 + Number(startingMinutes);
  const endTime = endHour * 60 + endMinutes;
  const end = `${endHour}:${endMinutes.toString().padStart(2, "0")}`;
  return {
    ...e,
    startingHour: Number(startingHour),
    startingMinutes: Number(startingMinutes),
    end,
    endHour,
    endMinutes,
    startTime,
    endTime,
  };
};

const colors = ["#931621", "#28464B", "#326771", "#2C8C99", "#42D9C8"];
const randomBackgroundColor = () =>
  colors[Math.floor(Math.random() * colors.length)];

export {
  randomBackgroundColor,
  sortEvent,
  extractEventTimes,
  extractOverlap,
  setDisplayQuotient,
  setDisplayProperty,
};
