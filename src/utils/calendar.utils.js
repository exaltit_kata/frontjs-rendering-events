export const generateSections = (limit) => {
  const sections = [];
  for (let i = limit.minHour; i <= limit.maxHour; i++) {
    sections.push({ id: i, label: `${i}:00` });
  }
  sections.pop();
  return sections;
};
