import React from "react";

import CalendarContainer from "./CalendarContainer";
import CalendarEventsContainer from "./CalendarEventsContainer";
import {
  extractEventTimes,
  sortEvent,
  extractOverlap,
  setDisplayQuotient,
} from "./../utils/event.utils";
import useWindowSize from "../utils/hooks/useWindowSize";

import rawevents from "./../data/input.json";
import "../css/App.css";

const Calendar = () => {
  const { height: windowHeight, width: windowWidth } = useWindowSize();

  const events = rawevents
    .map(extractEventTimes)
    .sort(sortEvent)
    .map(extractOverlap)
    .reduce(setDisplayQuotient);
  const minHour = events[0].startingHour;
  const maxHour = 21;
  const hourHeight = windowHeight / (maxHour - minHour);
  return (
    <div id="calendar" style={{ height: windowHeight, width: windowWidth }}>
      <CalendarContainer
        sectionDimension={{ height: hourHeight, width: windowWidth }}
        windowWidth={windowWidth}
        windowHeight={windowHeight}
        limit={{ minHour, maxHour }}
      />
      <CalendarEventsContainer
        events={events}
        windowWidth={windowWidth}
        windowHeight={windowHeight}
        hourHeight={hourHeight}
        limit={{ minHour, maxHour }}
      />
    </div>
  );
};

export default Calendar;
