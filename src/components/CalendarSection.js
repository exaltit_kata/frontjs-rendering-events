import React from "react";

import "../css/App.css";

const CalendarSection = ({ sectionDimension, label }) => {
  return (
    <tr
      className="calendar-section"
      style={{ height: sectionDimension.height, width: sectionDimension.width }}
    >
      <td className="calendar-section-label">
        <span>{label}</span>
        {
          //lastLabel && <span className="last">{lastLabel}</span>}
        }
      </td>
      <td className="calendar-section-slot"></td>
    </tr>
  );
};
export default CalendarSection;
