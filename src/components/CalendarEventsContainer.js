import React from "react";

import Event from "./Event";

import { setDisplayProperty } from "./../utils/event.utils";
import "../css/App.css";

const CalendarEventsContainer = ({
  events,
  windowWidth,
  windowHeight,
  hourHeight,
  limit,
}) => {
  const { minHour } = limit;
  events.forEach((e) =>
    setDisplayProperty(e, hourHeight, minHour, windowWidth)
  );

  return (
    <div
      key="calendar-events-container"
      style={{ height: windowHeight, width: windowWidth * 0.949 }}
      id="calendar-events-container"
    >
      {events.map((event) => {
        return <Event key={`event-${event.id}`} event={event} />;
      })}
    </div>
  );
};
export default CalendarEventsContainer;
