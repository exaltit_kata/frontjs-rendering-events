import React from "react";

import { randomBackgroundColor } from "./../utils/event.utils";
import "../css/App.css";

const Event = ({ event }) => {
  const backgroundColor = randomBackgroundColor();
  const { width, height, top, left } = event;

  return (
    <div
      className="event"
      style={{ width, height, top, left, backgroundColor }}
    >
      <div className="event-label">
        <p>
          {event.start} - {event.end}
        </p>
      </div>
    </div>
  );
};
export default Event;
