import React from "react";

import CalendarSection from "./CalendarSection";

import { generateSections } from "./../utils/calendar.utils";
import "../css/App.css";

const CalendarContainer = ({
  sectionDimension,
  limit,
  windowHeight,
  windowWidth,
}) => {
  const sections = generateSections(limit);
  return (
    <table cellSpacing={0} style={{ height: windowHeight, width: windowWidth }}>
      <tbody>
        {sections.map((section) => (
          <CalendarSection
            sectionDimension={sectionDimension}
            key={section.id}
            label={section.label}
            lastLabel={section.lastlabel}
          />
        ))}
      </tbody>
    </table>
  );
};
export default CalendarContainer;
